wt = 10;

translate([1500/2, 2000/2, 1000/2])
{
    difference()
    {
        cube([1500, 2000, 1000], center=true);
        translate([0, 0, wt])
        cube([1500-wt, 2000-wt, 1000], center=true);
    }

    translate([-500, 600, 0])
    cube([200, 200, 1000], center=true);

    translate([300, 200, 0])
    cube([200, 200, 1000], center=true);

    translate([-400, -400, 0])
    cube([200, 200, 1000], center=true);

    translate([50, -20, 0])
    cylinder(d = 100, h=1000, center=true);
}
