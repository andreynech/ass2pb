# Overview
Reads geometry data from files supported by 
[ASSIMP library](http://assimp.sourceforge.net) and stores it in 
[protobuf] (https://developers.google.com/protocol-buffers/) format. Only 
vertices, faces and normals are read and serialized in protobuf format.

# Usage
`ass2pb <input_file> <output_file>` where input_file is any file supported by 
assimp library. Output_file is serialized protobuf message. Corresponding proto
file is in the [proto] (proto) directrory. For example: 
`ass2pb data/monkey.obj monkey.pb`

There are two wavefront obj files included in the [data] (data) directory. Also,
there are corresponding .pb files generated with ass2pb. It is possible to dump
.pb file in plain text using the following command:
`cat monkey.pb | protoc --decode=Mesh geometry.proto > monkey.js`

# Compile
[Protobuf] (https://developers.google.com/protocol-buffers/) development package
(protoc, headers and libraries) should be installed on your system. We are
using [CMake ](https://cmake.org/) which should also be available.

After cloning the repository, please do not forget to initialize git
submodules with the following command: `git submodule update --init`. It will
pull the latest version of the ASSIMP library.

Standard CMake process could be used to build ass2pb, i.e. in ass2pb directory: 
`mkdir build && cd build && cmake .. && make` . On Windows only Release version
could be built out of the box. Some tweaking is required to build Debug version.