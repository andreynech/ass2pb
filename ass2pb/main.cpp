#include <iostream>
#include <fstream>

#include <assimp/Importer.hpp>  // C++ importer interface
#include <assimp/scene.h>       // Output data structure
#include <assimp/postprocess.h> // Post processing flags

#include "geometry.pb.h"


int main(int argc, char *argv[])
{
    // Verify that the version of the library that we linked against is
    // compatible with the version of the headers we compiled against.
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    if(argc != 3)
    {
        std::cerr << "Usage: ass2pb <3d_asset_file> <protobuf_file>\n";
        return 1;
    }

    // Create an instance of the Importer class
    Assimp::Importer importer;
    // And have it read the given file with postprocessing
    const aiScene* scene = importer.ReadFile(argv[1], 
                //aiProcess_FlipWindingOrder       |
                //aiProcess_CalcTangentSpace       | 
                aiProcess_Triangulate            |
                aiProcess_JoinIdenticalVertices  |

                //aiProcess_PreTransformVertices   |
                aiProcess_OptimizeGraph          |
                aiProcess_OptimizeMeshes         |

                aiProcess_FixInfacingNormals     |
                aiProcess_GenSmoothNormals       |

                aiProcess_SortByPType);

    // If the import failed, report it
    if(!scene)
    {
        std::cerr << importer.GetErrorString() << '\n';
        return 2;
    }

    std::cout << argv[1] << " read successfully\n";
    std::cout << "Root node name: " << scene->mRootNode->mName.C_Str() << '\n';
    std::cout << "Mesh count: " << scene->mNumMeshes << "\n";

    unsigned int idx_offset = 0;

    Mesh out_mesh;
    out_mesh.set_name(scene->mRootNode->mName.C_Str());

    for(unsigned int i = 0; i < scene->mNumMeshes; ++i)
    {
        const aiMesh &mesh = *(scene->mMeshes[i]);
        std::cout << "Mesh: " << mesh.mName.C_Str() << '\n';
        std::cout << "Verticess to process: " << mesh.mNumVertices << '\n';
        for(unsigned int v = 0; v < mesh.mNumVertices; ++v)
        {
            Vertex *new_vertex = out_mesh.add_vertex();

            Vec3 *pos = new_vertex->mutable_position();
            pos->set_x(mesh.mVertices[v].x);
            pos->set_y(mesh.mVertices[v].y);
            pos->set_z(mesh.mVertices[v].z);

            Vec3 *norm = new_vertex->mutable_normal();
            norm->set_x(mesh.mNormals[v].x);
            norm->set_y(mesh.mNormals[v].y);
            norm->set_z(mesh.mNormals[v].z);
        }
        for(unsigned int f = 0; f < mesh.mNumFaces; ++f)
        {
            const aiFace &face = mesh.mFaces[f];
            assert(face.mNumIndices == 3);

            Face *new_face = out_mesh.add_face();
            new_face->set_vertex_a(face.mIndices[0] + idx_offset);
            new_face->set_vertex_b(face.mIndices[1] + idx_offset);
            new_face->set_vertex_c(face.mIndices[2] + idx_offset);
        }
        idx_offset += mesh.mNumVertices;
    }

    {
        // Write the new mesh to file.
        std::fstream output(argv[2], std::ios::out | std::ios::trunc | std::ios::binary);
        if(!out_mesh.SerializeToOstream(&output))
        {
            std::cerr << "Failed to write" << argv[2] << '\n';
            return 3;
        }
    }

    // Optional:  Delete all global objects allocated by libprotobuf.
    google::protobuf::ShutdownProtobufLibrary();

    return 0;
}

